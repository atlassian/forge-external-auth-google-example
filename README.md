# Forge External Auth Google Example

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE)

This is an example [Forge](https://developer.atlassian.com/platform/forge/) app that uses Google's APIs to display a photo in Confluence.

See [developer.atlassian.com/platform/forge/use-an-external-oauth-2.0-api-with-fetch/](https://developer.atlassian.com/platform/forge/use-an-external-oauth-2.0-api-with-fetch/) for documentation and tutorials explaining using OAuth APIs with Forge.

![Screenshot](./screenshot.png)

## Requirements

1. See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.
2. Setup an OAuth client ID in Google Console using [this guide](https://developer.atlassian.com/platform/forge/use-an-external-oauth-2.0-api-with-fetch/#google-console-setup).
3. Enable the [Photos API](https://console.cloud.google.com/apis/library/photoslibrary.googleapis.com) in your Google project.

## Quick start

- Modify `manifest.yml` with your Client ID.

- Register a new App ID by running:
```
forge register
```

- Build and deploy your app by running:
```
forge deploy
```

- Set the client secret by running:
```
forge providers configure
```

- Install your app in an Atlassian site by running:
```
forge install
```

- Develop your app by running `forge tunnel` to proxy invocations locally:
```
forge tunnel
```

### Notes
- Use the `forge deploy` command when you want to persist code changes.
- Use the `forge install` command when you want to install the app on a new site.
- Once the app is installed on a site, the site picks up the new app changes you deploy without needing to rerun the install command.

### Deploying to production

- To promote your code to the production environment, run `forge deploy -e production`
- And set the production secret by running `forge providers configure -e production`

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.

## License

Copyright (c) 2021 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![From Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers.png)](https://www.atlassian.com)
