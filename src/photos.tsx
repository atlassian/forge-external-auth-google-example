import ForgeUI, {Code, Fragment, Heading, Image, Table, Row, Cell, useEffect, useState} from "@forge/ui";
import api from '@forge/api';
import {MediaItem, RequestError} from "./types";

function chunk<T>(input: Array<T>, size: number): Array<Array<T>> {
  const result = [];
  for (let i = 0; i < input.length; i += size) {
    result.push(input.slice(i, i+size));
  }
  return result;
}

export const PhotosList = () => {
  const [images, setImages] = useState<Array<MediaItem>|RequestError>(null);
  useEffect(async () => {
    const response = await api.asUser()
      .withProvider('google', 'google-photos')
      .fetch('/v1/mediaItems');
    if (response.ok) {
      const data = await response.json();
      setImages(data.mediaItems);
    } else {
      setImages({
        status: response.status,
        statusText: response.statusText,
        text: await response.text(),
      });
    }
  }, []);

  const imageMap = Array.isArray(images) && chunk(images, 3);

  return (
    <Fragment>
      <Heading>My Google Photos</Heading>
      {!Array.isArray(images) && (
        <Code text={JSON.stringify(images, null, 2)} language="json" showLineNumbers />
      )}
      {imageMap && (
        <Table>
          {imageMap.map((imageRow, idx) => (
            <Row key={idx}>
              {imageRow.map((image) => (
                <Photo key={image.id} {...image} />
              ))}
            </Row>
          ))}
        </Table>
      )}
    </Fragment>
  );
};

const Photo = (image: MediaItem) => {
  return (
    <Cell>
      <Image src={`${image.baseUrl}=w237-h158`} alt={image.filename} />
    </Cell>
  );
}
