export interface GoogleProfile {
  id: string,
  email: string,
  verified_email: boolean,
  name: string,
  given_name: string,
  family_name: string,
  picture: string,
  locale: string,
  hd: string,
}

export interface CalenderList {
  items: {
    kind: string
    id: string
    summary: string
    primary?: boolean
  }[]
}

export interface MediaItem {
  id: string
  filename: string
  description?: string
  productUrl: string
  baseUrl: string
  mimeType: string
  mediaMetadata: {
    creationTime: string
    width: string
    height: string
    photo?: {}
    video?: {}
  }
  contributorInfo: any
}

export interface RequestError {
  status: number
  statusText: string
  text: string
}

export const test = 5
