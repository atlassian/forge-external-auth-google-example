import ForgeUI, {Code, Em, Fragment, Button, Image, Tab, Tabs, Text, ModalDialog, useState, useEffect} from "@forge/ui";
import api from '@forge/api';
import {GoogleProfile, RequestError} from "./types";

export const ProfileInformation = () => {
  const [isOpen, setOpen] = useState(false);
  return (
    <Fragment>
      <Button text="Show Profile" onClick={() => setOpen(true)} />
      {isOpen && (
        <ModalDialog
          header="Google Profile"
          closeButtonText="Close"
          onClose={() => setOpen(false)}
        >
          <ProfileDisplay />
        </ModalDialog>
      )}
    </Fragment>
  );
}


const ProfileDisplay = () => {
  const [data, setData] = useState<GoogleProfile|RequestError|null>(null);
  useEffect(async () => {
    const response = await api.asUser()
      .withProvider('google', 'google-apis')
      .fetch('/userinfo/v2/me');
    if (response.ok) {
      setData(await response.json());
    } else {
      setData({
        status: response.status,
        statusText: response.statusText,
        text: await response.text(),
      });
    }
  }, []);
  return (
    <Tabs>
      {data && 'name' in data && (
        <Tab label="User Info">
          <Fragment>
            <Text>
              <Em>{data.name}</Em> ({data.email})
            </Text>
            <Image src={data.picture} alt={data.name} />
          </Fragment>
        </Tab>
      )}
      <Tab label="JSON">
        <Code text={JSON.stringify(data, null, 2)} language="json" showLineNumbers />
      </Tab>
    </Tabs>
  );
}
