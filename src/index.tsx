import ForgeUI, { render, Fragment, Macro, useEffect } from '@forge/ui';
import api from '@forge/api';

import {PhotosList} from './photos';
import {ProfileInformation} from "./profile";


const App = () => {
  useEffect(async () => {
    const google = api.asUser().withProvider('google');
    // If the user has no credentials yet, trigger straight away
    if (!await google.hasCredentials()) {
      await google.requestCredentials();
    }
  }, []);
  return (
    <Fragment>
      <PhotosList />
      <ProfileInformation />
    </Fragment>
  );
};

export const run = render(
  <Macro
    app={<App />}
  />
);
